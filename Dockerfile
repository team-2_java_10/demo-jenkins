
FROM openjdk:11
EXPOSE 8080
ADD target/quizz.jar quizz.jar
ENTRYPOINT [ "java", "-jar" , "/quizz.jar" ]