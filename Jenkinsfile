pipeline {
    agent any

    environment {
        DOCKER_REGISTRY = 'https://index.docker.io/v1/'
        DOCKER_REPO = 'long2002/quizz'
    }

    tools {
        maven 'my-maven'
    }

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out source code...'
                checkout scm
            }
        }

        stage('Build') {
            steps {
                echo 'Building the application...'
                bat 'mvn clean install'
            }
        }

        stage('Test') {
            steps {
                echo 'Running tests...'
                bat 'mvn test'
            }
        }

        stage('Package') {
            steps {
                echo 'Packaging the application...'
                bat 'mvn package'
            }
        }

stage('Confirm Application') {
            steps {
                echo 'Running acceptance tests...'
                bat 'mvn verify'
            }
         }
        stage('Build and Push Docker Image') {
            steps {
                script {
                    echo 'Building Docker image...'
                    docker.withRegistry(DOCKER_REGISTRY, 'docker-demo') {
                        bat "docker build -t ${DOCKER_REPO} ."
                        bat "docker push ${DOCKER_REPO}"
                    }
                }
            }
        }

        stage('Deploy to Staging') {
            steps {
                echo 'Deploying to staging environment...'
                // Thực hiện bước triển khai đối với môi trường staging
                // Có thể sử dụng Docker Compose hoặc công cụ triển khai khác
            }
        }

        stage('Security Scan') {
            steps {
                echo 'Running security scans...'
                // Thực hiện kiểm thử bảo mật, ví dụ sử dụng OWASP ZAP
            }
        }


        stage('Remove Docker Images') {
            steps {
                script {
                    echo 'Removing Docker images...'
                   
                    def imageIds = powershell(script: "docker images -q ${DOCKER_REPO}", returnStdout: true).trim().split("\n")
 
                   
                    for (def id in imageIds) {
                        powershell(script: "docker rmi $id", returnStatus: true)
                    }
                }
            }
        }
    }
}
