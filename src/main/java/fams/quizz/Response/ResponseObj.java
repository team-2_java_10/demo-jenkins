package fams.quizz.Response;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class ResponseObj {
    private  Integer id;
    private  String response;


}
